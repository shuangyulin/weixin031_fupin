由于APP软件在开发以及运营上面所需成本较高，而用户手机需要安装各种APP软件，因此占用用户过多的手机存储空间，导致用户手机运行缓慢，体验度比较差，进而导致用户会卸载非必要的APP，倒逼管理者必须改变运营策略。随着微信小程序的出现，解决了用户非独立APP不可访问内容的痛点，所以很多APP软件都转向微信小程序。本次课题就运用了微信小程序技术开发一个沈阳工业大学助农扶贫微信小程序。

本次使用数据库工具MySQL以及微信开发者工具开发的沈阳工业大学助农扶贫微信小程序，可以实现目标用户群需要的功能，其中管理员管理注册农户以及注册用户的信息，统计订单，管理商品，商品评价，商品退货等信息。农户管理销售的商品，对订单商品进行发货，管理配送订单，管理商品评价，审核商品退货信息。用户收藏商品，购买商品，支付订单，申请商品退货，评价订单商品。

总之，沈阳工业大学助农扶贫微信小程序可以更加方便用户购买农户销售的商品，也让农户方便管理销售的商品，管理商品订单以及订单配送信息，该平台不仅能够帮助农户销售农产品，还能增加农户的收入。

关键词：沈阳工业大学助农扶贫微信小程序；订单；商品；退货
